-- 当站点开启了用户分表时，需要执行以下 SQL 语句

ALTER TABLE pre_common_member_status_archive
    MODIFY COLUMN regip VARCHAR(45) NOT NULL DEFAULT '',
    MODIFY COLUMN lastip VARCHAR(45) NOT NULL DEFAULT '',
    ADD COLUMN regport SMALLINT(6) unsigned NOT NULL DEFAULT '0' AFTER lastip;

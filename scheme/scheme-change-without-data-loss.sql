ALTER TABLE pre_common_admincp_session 
    MODIFY COLUMN ip VARCHAR(45) NOT NULL DEFAULT '';

ALTER TABLE pre_common_credit_log 
    MODIFY COLUMN logid INT(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE pre_common_banned 
    ADD COLUMN ip VARCHAR(49) NOT NULL DEFAULT '' AFTER id, 
    ADD COLUMN upperip VARBINARY(16) NOT NULL DEFAULT 0x0 AFTER ip,
    ADD COLUMN lowerip VARBINARY(16) NOT NULL DEFAULT 0x0 AFTER ip, 
    ADD INDEX iprange (lowerip, upperip);

ALTER TABLE pre_common_credit_log 
    MODIFY COLUMN logid INT(8) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE pre_common_failedip 
    MODIFY COLUMN ip VARCHAR(45) NOT NULL DEFAULT '';

ALTER TABLE pre_common_failedlogin 
    MODIFY COLUMN ip VARCHAR(45) NOT NULL DEFAULT '';

ALTER TABLE pre_common_invite 
    MODIFY COLUMN inviteip VARCHAR(45) NOT NULL DEFAULT '';

ALTER TABLE pre_common_member_action_log 
    MODIFY COLUMN id INT(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE pre_common_member_status
    MODIFY COLUMN regip VARCHAR(45) NOT NULL DEFAULT '',
    MODIFY COLUMN lastip VARCHAR(45) NOT NULL DEFAULT '',
    ADD COLUMN regport SMALLINT(6) unsigned NOT NULL DEFAULT '0' AFTER lastip;

ALTER TABLE pre_common_regip 
    MODIFY COLUMN ip VARCHAR(45) NOT NULL DEFAULT '';

ALTER TABLE pre_common_remote_port 
    MODIFY COLUMN useip VARCHAR(45) NOT NULL DEFAULT '';

ALTER TABLE pre_common_searchindex 
    MODIFY COLUMN useip VARCHAR(45) NOT NULL DEFAULT '';

ALTER TABLE pre_common_session 
    ADD COLUMN ip VARCHAR(45) NOT NULL DEFAULT '' AFTER sid,
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_common_task
    ADD COLUMN exclusivetaskid smallint(6) unsigned NOT NULL DEFAULT '0' AFTER relatedtaskid,
    MODIFY COLUMN applicants INT(10) unsigned NOT NULL DEFAULT '0',
    MODIFY COLUMN achievers INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_common_usergroup_field 
    ADD COLUMN allowsavenum int(10) unsigned NOT NULL DEFAULT '0' AFTER allowat,
    ADD COLUMN allowsavereply tinyint(1) unsigned NOT NULL DEFAULT '1' AFTER allowat,
    ADD COLUMN allowsave tinyint(1) unsigned NOT NULL DEFAULT '1' AFTER allowat,
    MODIFY COLUMN edittimelimit INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_common_visit 
    MODIFY COLUMN ip VARCHAR(45) NOT NULL DEFAULT '';

ALTER TABLE pre_connect_feedlog 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_connect_tthreadlog 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_activity 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0',
    MODIFY COLUMN aid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_activityapply 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_attachment 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL AUTO_INCREMENT,
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_attachment_0 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL,
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_attachment_1 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL,
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_attachment_2 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL,
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_attachment_3 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL,
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_attachment_4 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL,
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_attachment_5 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL,
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_attachment_6 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL,
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_attachment_7 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL,
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_attachment_8 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL,
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_attachment_9 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL,
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_attachment_exif 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL;

ALTER TABLE pre_forum_attachment_unused 
    MODIFY COLUMN aid INT(10) unsigned NOT NULL;

ALTER TABLE pre_forum_collectioncomment 
    MODIFY COLUMN useip VARCHAR(45) NOT NULL DEFAULT '';

ALTER TABLE pre_forum_collectionteamworker 
    MODIFY COLUMN lastvisit INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_collectionthread 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_debate 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_debatepost 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_filter_post 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_forumfield 
    MODIFY COLUMN livetid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_forumrecommend 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL,
    MODIFY COLUMN aid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_groupcreditslog 
    MODIFY COLUMN logdate INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_hotreply_member 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_hotreply_number 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_memberrecommend 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL;

ALTER TABLE pre_forum_newthread 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_order 
    MODIFY COLUMN ip VARCHAR(45) NOT NULL DEFAULT '',
    ADD COLUMN `port` SMALLINT(6) unsigned NOT NULL DEFAULT '0' AFTER ip;

ALTER TABLE pre_forum_poll 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_polloption 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_polloption_image 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_pollvoter 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_post 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0',
    MODIFY COLUMN useip VARCHAR(45) NOT NULL DEFAULT '',
    MODIFY COLUMN position INT(10) unsigned NOT NULL;

ALTER TABLE pre_forum_post_location 
    MODIFY COLUMN tid INT(10) unsigned DEFAULT '0';

ALTER TABLE pre_forum_postcache
    MODIFY COLUMN `comment` MEDIUMTEXT NOT NULL,
    MODIFY COLUMN rate MEDIUMTEXT NOT NULL;

ALTER TABLE pre_forum_postcomment 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0',
    MODIFY COLUMN useip VARCHAR(45) NOT NULL DEFAULT '';

ALTER TABLE pre_forum_postlog 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_poststick 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL;

ALTER TABLE pre_forum_promotion 
    MODIFY COLUMN ip VARCHAR(45) NOT NULL DEFAULT '',
    ADD COLUMN `port` SMALLINT(6) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_relatedthread 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_replycredit 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL,
    MODIFY COLUMN extcredits INT(10) unsigned NOT NULL DEFAULT '0',
    MODIFY COLUMN `times` INT(10) unsigned NOT NULL DEFAULT '0',
    MODIFY COLUMN membertimes INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_rsscache 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_sofa 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_thread 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL AUTO_INCREMENT,
    MODIFY COLUMN maxposition INT(10) unsigned NOT NULL DEFAULT '0',
    MODIFY COLUMN replycredit INT(10) NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_threadaddviews 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_threadclosed 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_threaddisablepos 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_threadhidelog 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_threadhot 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_threadimage 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_threadlog 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_threadmod 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_threadpartake 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_threadpreview 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_threadrush 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_forum_trade 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL,
    MODIFY COLUMN aid INT(10) unsigned NOT NULL;

ALTER TABLE pre_forum_tradelog 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL;

ALTER TABLE pre_forum_typeoptionvar 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_home_comment 
    MODIFY COLUMN ip VARCHAR(45) NOT NULL DEFAULT '';

ALTER TABLE pre_home_docomment 
    MODIFY COLUMN ip VARCHAR(45) NOT NULL DEFAULT '',
    ADD COLUMN `port` SMALLINT(6) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_home_doing 
    MODIFY COLUMN ip VARCHAR(45) NOT NULL DEFAULT '';

ALTER TABLE pre_home_follow_feed 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_home_follow_feed_archiver 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE pre_home_notification 
    MODIFY COLUMN id BIGINT(20) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE pre_security_evilpost 
    MODIFY COLUMN tid INT(10) unsigned NOT NULL DEFAULT '0';
